Source: backports-dkms
Section: kernel
Priority: optional
Maintainer: You-Sheng Yang <vicamo@gmail.com>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dkms,
 quilt,
Standards-Version: 4.5.1
Homepage: https://gitlab.com/vicamo/backports-dkms
Vcs-Browser: https://gitlab.com/vicamo/backports-dkms/tree/master
Vcs-Git: https://gitlab.com/vicamo/backports-dkms.git

Package: backports-ar5523-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: ar5523-modules
Description: ar5523 driver backport in DKMS format
 This module add support for AR5523 based USB dongles such as D-Link DWL-G132,
 Netgear WPN111 and many more.
 .
 This package provides ar5523 modules built from package release by
 kernel.org backports.

Package: backports-ath10k-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: ath10k-modules
Description: ath10k driver backport in DKMS format
 This module adds support for wireless adapters based on Atheros IEEE 802.11ac
 family of chipsets.
 .
 This package provides ath10k modules built from package release by
 kernel.org backports.

Package: backports-ath5k-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: ath5k-modules
Description: ath5k driver backport in DKMS format
 This module adds support for wireless adapters based on Atheros 5xxx chipset.
 .
 Currently the following chip versions are supported:
 .
  * MAC: AR5211 AR5212
  * PHY: RF5111/2111 RF5112/2112 RF5413/2413
 .
 This package provides ath5k modules built from package release by
 kernel.org backports.

Package: backports-ath6kl-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: ath6kl-modules
Description: ath6kl driver backport in DKMS format
 This module adds core support for wireless adapters based on Atheros AR6003
 and AR6004 chipsets. You still need separate bus drivers for USB and SDIO to
 be able to use real devices.
 .
 This package provides ath6kl modules built from package release by
 kernel.org backports.

Package: backports-ath9k-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: ath9k-modules
Description: ath9k driver backport in DKMS format
 This module adds support for wireless adapters based on
 Atheros IEEE 802.11n AR5008, AR9001 and AR9002 family
 of chipsets. For a specific list of supported external
 cards, laptops that already ship with these cards and
 APs that come with these cards refer to ath9k wiki
 products page:
 .
 https://wireless.wiki.kernel.org/en/users/Drivers/ath9k/products
 .
 This package provides ath9k modules built from package release by
 kernel.org backports.

Package: backports-b43-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: b43-modules
Description: b43 driver backport in DKMS format
 b43 is a driver for the Broadcom 43xx series wireless devices.
 .
 Check "lspci" for something like
 "Broadcom Corporation BCM43XX 802.11 Wireless LAN Controller"
 to determine whether you own such a device.
 .
 This driver supports the new BCM43xx IEEE 802.11G devices, but not
 the old IEEE 802.11B devices. Old devices are supported by
 the b43legacy driver.
 Note that this has nothing to do with the standard that your AccessPoint
 supports (A, B, G or a combination).
 IEEE 802.11G devices can talk to IEEE 802.11B AccessPoints.
 .
 It is safe to include both b43 and b43legacy as the underlying glue
 layer will automatically load the correct version for your device.
 .
 This driver uses V4 firmware, which must be installed separately using
 b43-fwcutter.
 .
 This package provides b43 modules built from package release by
 kernel.org backports.

Package: backports-b43legacy-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: b43legacy-modules
Description: b43legacy driver backport in DKMS format
 b43legacy is a driver for 802.11b devices from Broadcom (BCM4301 and
 BCM4303) and early model 802.11g chips (BCM4306 Ver. 2) used in the
 Linksys WPC54G V1 PCMCIA devices.
 .
 Newer 802.11g and 802.11a devices need b43.
 .
 It is safe to include both b43 and b43legacy as the underlying glue
 layer will automatically load the correct version for your device.
 .
 This driver uses V3 firmware, which must be installed separately using
 b43-fwcutter.
 .
 This package provides b43legacy modules built from package release by
 kernel.org backports.

Package: backports-brcmfmac-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: brcmfmac-modules
Description: brcmfmac driver backport in DKMS format
 This module adds support for wireless adapters based on Broadcom
 FullMAC chipsets.
 .
 This package provides brcmfmac modules built from package release by
 kernel.org backports.

Package: backports-brcmsmac-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: brcmsmac-modules
Description: brcmsmac driver backport in DKMS format
 This module adds support for PCIe wireless adapters based on Broadcom
 IEEE802.11n SoftMAC chipsets.
 .
 This package provides brcmsmac modules built from package release by
 kernel.org backports.

Package: backports-carl9170-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: carl9170-modules
Description: carl9170 driver backport in DKMS format
 This is the mainline driver for the Atheros "otus" 802.11n USB devices.
 .
 It needs a special firmware (carl9170-1.fw), which can be downloaded
 from following wiki:
 <https://wireless.wiki.kernel.org/en/users/Drivers/carl9170>
 .
 This package provides carl9170 modules built from package release by
 kernel.org backports.

Package: backports-cw1200-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: cw1200-modules
Description: cw1200 driver backport in DKMS format
 This is a driver for the ST-E CW1100 & CW1200 WLAN chipsets.
 .
 This package provides cw1200 modules built from package release by
 kernel.org backports.

Package: backports-hwsim-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: hwsim-modules
Description: hwsim driver backport in DKMS format
 This driver is a developer testing tool that can be used to test
 IEEE 802.11 networking stack (mac80211) functionality. This is not
 needed for normal wireless LAN usage and is only for testing. See
 Documentation/networking/mac80211_hwsim for more information on how
 to use this tool.
 .
 This package provides hwsim modules built from package release by
 kernel.org backports.

Package: backports-iwlwifi-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: iwlwifi-modules
Description: iwlwifi driver backport in DKMS format
 Intel Wireless Wi-Fi Link Next-Gen AGN
 .
 This option enables support for use with the following hardware:
 .
  * Intel Wireless Wi-Fi Link 6250AGN Adapter
  * Intel 6000 Series Wi-Fi Adapters (6200AGN and 6300AGN)
  * Intel Wi-Fi Link 1000BGN
  * Intel Wireless Wi-Fi 5150AGN
  * Intel Wireless Wi-Fi 5100AGN, 5300AGN, and 5350AGN
  * Intel 6005 Series Wi-Fi Adapters
  * Intel 6030 Series Wi-Fi Adapters
  * Intel Wireless Wi-Fi Link 6150BGN 2 Adapter
  * Intel 100 Series Wi-Fi Adapters (100BGN and 130BGN)
  * Intel 2000 Series Wi-Fi Adapters
  * Intel 7260 Wi-Fi Adapter
  * Intel 3160 Wi-Fi Adapter
  * Intel 7265 Wi-Fi Adapter
  * Intel 8260 Wi-Fi Adapter
  * Intel 3165 Wi-Fi Adapter
 .
 This driver uses the kernel's mac80211 subsystem.
 .
 In order to use this driver, you will need a firmware
 image for it. You can obtain the microcode from:
 .
 <https://wireless.wiki.kernel.org/en/users/Drivers/iwlwifi>.
 .
 The firmware is typically installed in /lib/firmware. You can
 look in the hotplug script /etc/hotplug/firmware.agent to
 determine which directory FIRMWARE_DIR is set to when the script
 runs.
 .
 This package provides iwlwifi modules built from package release by
 kernel.org backports.

Package: backports-libertas-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: libertas-modules
Description: libertas driver backport in DKMS format
 A driver for Marvell Libertas 8xxx devices.
 .
 This package provides libertas modules built from package release by
 kernel.org backports.

Package: backports-libertas-tf-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: libertas-tf-modules
Description: libertas_tf driver backport in DKMS format
 A driver for Marvell Libertas 8xxx devices using thinfirm.
 .
 This package provides libertas_tf modules built from package release by
 kernel.org backports.

Package: backports-mwifiex-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: mwifiex-modules
Description: mwifiex driver backport in DKMS format
 This adds support for wireless adapters based on Marvell
 802.11n/ac chipsets.
 .
 This package provides mwifiex modules built from package release by
 kernel.org backports.

Package: backports-mwl8k-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: mwl8k-modules
Description: mwl8k driver backport in DKMS format
 This driver supports Marvell TOPDOG 802.11 wireless cards.
 .
 This package provides mwl8k modules built from package release by
 kernel.org backports.

Package: backports-rtlwifi-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: rtlwifi-modules
Description: rtlwifi driver backport in DKMS format
 This driver will enable support for the Realtek mac80211-based
 wireless drivers. Drivers rtl8192ce, rtl8192cu, rtl8192se, rtl8192de,
 rtl8723ae, rtl8723be, rtl8188ee, rtl8192ee, and rtl8821ae share
 some common code.
 .
 This package provides rtlwifi modules built from package release by
 kernel.org backports.

#Package: backports-wcn36xx-dkms
#Architecture: all
#Depends:
# bison,
# dkms,
# flex,
# libc6-dev | libc-dev,
# ${misc:Depends},
#Provides: wcn36xx-modules
#Description: wcn36xx driver backport in DKMS format
# This module adds support for wireless adapters based on
# Qualcomm Atheros WCN3660 and WCN3680 mobile chipsets.
# .
# This package provides wcn36xx modules built from package release by
# kernel.org backports.

Package: backports-wil6210-dkms
Architecture: all
Depends:
 bison,
 dkms,
 flex,
 libc6-dev | libc-dev,
 ${misc:Depends},
Provides: wil6210-modules
Description: wil6210 driver backport in DKMS format
 This module adds support for wireless adapter based on
 wil6210 chip by Wilocity. It supports operation on the
 60 GHz band, covered by the IEEE802.11ad standard.
 .
 https://wireless.wiki.kernel.org/en/users/Drivers/wil6210
 .
 This package provides wil6210 modules built from package release by
 kernel.org backports.
